

((clojure-mode
  (clojure-indent-style                . :always-indent)
  (cider-preferred-build-tool          . clojure-cli)
  (cider-default-cljs-repl             . figwheel-main)
  (cider-figwheel-main-default-options . "dev")
  (clojure-align-forms-automatically   . 1))
 (clojurec-mode
  (cider-default-cljs-repl . figwheel-main)))

;; This file is part of unicorn.

;; unicorn is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; unicorn is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with unicorn.  If not, see <http://www.gnu.org/licenses/>.
