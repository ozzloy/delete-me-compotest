
((clojure-mode
  (clojure-indent-style                . :always-indent)
  (cider-preferred-build-tool          . clojure-cli)
  (cider-default-cljs-repl             . figwheel-main)
  (cider-figwheel-main-default-options . "pathfinderwebsockets")
  (clojure-align-forms-automatically   . 1))
 (clojurec-mode
  (cider-default-cljs-repl . figwheel-main)))
