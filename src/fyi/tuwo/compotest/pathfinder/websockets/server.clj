(ns fyi.tuwo.compotest.pathfinder.websockets.server
  (:require
   [clojure.pprint :refer [pprint]]
   [clojure.test :refer [function?]]
   [compojure.core :refer [defroutes GET POST]]
   [hiccup.core :refer [h]]
   [hiccup.page :refer [html5 include-js]]
   [org.httpkit.server :refer [run-server]]
   [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
   [ring.middleware.reload :refer [wrap-reload]]
   [taoensso.sente
    :refer [make-channel-socket! start-server-chsk-router!]]
   [taoensso.sente.server-adapters.http-kit :refer [get-sch-adapter]]))

(def dev? true)

(defonce app-state (atom {:callable-cells (shuffle (range 75))}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(let [{:keys [ch-recv send-fn connected-uids
              ajax-post-fn ajax-get-or-ws-handshake-fn]}
      (make-channel-socket! (get-sch-adapter) {})]

  (def ring-ajax-post                ajax-post-fn)
  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)

  ;; ChannelSocket's receive channel
  (def ch-chsk                       ch-recv)

  ;; ChannelSocket's send API fn
  (def chsk-send!                    send-fn)

  ;; Watchable, read-only atom
  (def connected-uids                connected-uids))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn handle-home [http-request]
  (html5
    {:lang "en"}
    [:head
     [:meta {:charset "UTF-8"}]
     [:meta {:name    "viewport"
             :content "width=device-width, initial-scale=1"}]
     [:title "this is a title"]]
    [:body
     [:div#sente-csrf-token
      {:data-csrf-token (:anti-forgery-token http-request)}]
     [:div#app [:h1 "loading..."]]
     [:div
      [:h1 "original request"]
      [:pre (-> http-request pprint with-out-str h)]]
     (include-js (if dev?
                   "/cljs-out/pathfinderwebsockets-main.js"
                   "main.js"))]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defroutes http-routes
  (GET "/" http-request (handle-home http-request))
  (GET "/chsk" http-request
    (ring-ajax-get-or-ws-handshake http-request))
  (POST "/chsk" http-request
    (ring-ajax-post http-request)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def http-router
  (wrap-defaults http-routes site-defaults))

(defonce ?stop-http-server! (atom nil))

(defn stop-http-server! []
  (when (function? @?stop-http-server!)
    (@?stop-http-server! :timeout 100)
    (reset! ?stop-http-server! nil)))

(defn start-http-server! [& [port]]
  (stop-http-server!)
  (let [port                 (or port 0)
        top-handler          (if dev?
                               (wrap-reload (var http-router))
                               http-router)
        http-server-stop-fn! (run-server top-handler {:port port})
        port                 (:local-port (meta http-server-stop-fn!))
        uri                  (str "http://localhost:" port)]
    (if dev?
      (try
        (.browse (java.awt.Desktop/getDesktop) (java.net.URI. uri))
        (catch java.awt.HeadlessException _)))
    (println (str "http server running at " uri))
    (reset! ?stop-http-server! http-server-stop-fn!)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn call-next-cell [app-state]
  (-> app-state
    (update :called-cells conj (first (:callable-cells app-state)))
    (update :callable-cells #(drop 1 %))))

(defn call-next-cell! [app-state]
  (swap! app-state call-next-cell))

(defn handle-ping [event-message]
  ".")

(defn handle-call 
  [{:as   event-message
    :keys [?reply-fn client-id connected-uids uid event id ch-recv]}]
  (call-next-cell! app-state)
  (when (fn? ?reply-fn) (?reply-fn @app-state))
  "handled call")

(defn handle-unknown 
  [{:as   event-message
    :keys [?reply-fn client-id connected-uids uid event id ch-recv]}]
  (str "\n(event-message-handler\n"
    (-> event-message pprint with-out-str)
    ")")
  (when (fn? ?reply-fn)
    (?reply-fn :data-from-server-in-response-to-message)))

(defn event-message-handler
  [{:as   event-message
    :keys [?reply-fn client-id connected-uids uid event id ch-recv]}]
  (cond
    (= :chsk/ws-ping id) (print (handle-ping event-message))
    (= :b1ng0/call   id) (println (handle-call event-message))
    :else                (println (handle-unknown event-message)))
  (flush))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defonce ?stop-websocket-router! (atom nil))

(defn stop-websocket-router! []
  (when (function? @?stop-websocket-router!)
    (@?stop-websocket-router!)
    (reset! ?stop-websocket-router! nil)))

(defn start-websocket-router! []
  (stop-websocket-router!)
  (reset! ?stop-websocket-router!
    (start-server-chsk-router! ch-chsk #'event-message-handler)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn start! []
  (start-http-server!)
  (start-websocket-router!))

(defonce start-once!
  (start!))

(comment

  (start!)

  (=
    (keys event-message)
    (:?reply-fn
     :ch-recv
     :client-id
     :connected-uids
     :uid
     :event
     :id
     :send-buffers
     :ring-req
     :?data
     :send-fn)))
