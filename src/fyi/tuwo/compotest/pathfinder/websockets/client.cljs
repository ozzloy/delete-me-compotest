(ns ^:figwheel-hooks fyi.tuwo.compotest.pathfinder.websockets.client
  (:refer-clojure :exclude [atom])
  (:require
   [cljs.pprint :refer [pprint]]
   [reagent.core :refer [atom]]
   [reagent.dom :refer [render]]
   [taoensso.sente
    :refer [cb-success?
            chsk-reconnect!
            make-channel-socket-client!
            start-client-chsk-router!]]))

(enable-console-print!)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def ?csrf-token
  (when-let [el (.getElementById js/document "sente-csrf-token")]
    (.getAttribute el "data-csrf-token")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn start-websocket! []
  (let [{:keys [chsk ch-recv send-fn state]}
        (make-channel-socket-client!
          "/chsk" ; Note the same path as before
          ?csrf-token
          {:type :auto ; e/o #{:auto :ajax :ws}
           })]

    (def chsk       chsk)

    ;; ChannelSocket's receive channel
    (def ch-chsk    ch-recv)

    ;; ChannelSocket's send API fn
    (def chsk-send! send-fn)

    ;; Watchable, read-only atom
    (def chsk-state state))
  true)

(defonce start-websocket-once!
  (start-websocket!))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn handle-ping [event-message]
  ".")

(defn handle-unknown [event-message]
  (str "\n(event-message-handler\n  "
    (-> event-message pprint with-out-str)
    ")"))

(defn event-message-handler [{:as   event-message
                              :keys [event id ?data]}]
  (cond
    (= [:chsk/ws-ping] ?data) (print (handle-ping event-message))
    :else                     (println (handle-unknown event-message)))
  (flush))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defonce ?stop-websocket-router! (atom nil))

(defn stop-websocket-router! []
  (when (fn? @?stop-websocket-router!)
    (@?stop-websocket-router!)
    (reset! ?stop-websocket-router! nil)))

(defn start-websocket-router! []
  (stop-websocket-router!)
  (reset! ?stop-websocket-router!
    (start-client-chsk-router! ch-chsk #'event-message-handler)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defonce web-ui-state (atom nil))

(defn called-cells-section [web-ui-state]
  [:div#called-cells-section
   [:h1 "called-cells"]
   (if-let [called-cells (:called-cells @web-ui-state)]
     (for [called-cell called-cells]
       ^{:key called-cell}
       [:span.called-cell called-cell])
     "no cells called yet")])

(defn home [web-ui-state]
  [:div
   [:h1 "hello from reagent"]
   [called-cells-section web-ui-state]])

(defn mount! [web-ui-state]
  (render [home web-ui-state] (js/document.getElementById "app")))

(defn ^:after-load re-render []
  (mount! web-ui-state))

(defn start! []
  (start-websocket-router!)
  (mount! web-ui-state))


(defonce start-once! (start!))


(comment
  (chsk-send!
    [:pathfinder/hello-test {:hello "world"}]
    5000
    #(pprint %1))

  (chsk-send!
    [:b1ng0/call]
    1000
    #(reset! web-ui-state %))

  (chsk-reconnect! chsk))
