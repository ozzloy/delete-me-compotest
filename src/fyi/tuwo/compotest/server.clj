(ns fyi.tuwo.compotest.server
  (:gen-class)
  (:require
   [clojure.test :refer [function?]]
   [clojure.pprint :refer [pprint]]
   [hiccup.page :refer [html5 include-js]]
   [hiccup.core :refer [h]]
   [org.httpkit.server :refer [run-server]]
   [compojure.core :refer [defroutes GET POST]]
   [compojure.route :refer [not-found]]
   [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
   [ring.middleware.resource :refer [wrap-resource]]
   [ring.middleware.content-type :refer [wrap-content-type]]
   [ring.middleware.keyword-params :refer [wrap-keyword-params]]
   [ring.middleware.params :refer [wrap-params]]
   [ring.middleware.anti-forgery
    :refer [wrap-anti-forgery *anti-forgery-token*]]
   [ring.middleware.session :refer [wrap-session]]
   [ring.middleware.reload :refer [wrap-reload]]
   [taoensso.sente :as sente :refer [start-server-chsk-router!]]
   [taoensso.sente.server-adapters.http-kit
    :refer (get-sch-adapter)]
   ;; TODO: decide what to do about format from clojure vs from encore
   #_[taoensso.encore :refer [format]]))

(def dev?
  ;; TODO: read this from env or config file
  true)

(let [{:keys [ch-recv send-fn connected-uids
              ajax-post-fn ajax-get-or-ws-handshake-fn]}
      (sente/make-channel-socket! (get-sch-adapter) {})]

  ;; (fn [ring-req])  for Ring CSRF-POST + chsk URL.
  (def ring-ajax-post ajax-post-fn)
  ;; (fn [ring-req])  for Ring GET + chsk URL.
  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)

  ;; ChannelSocket's receive channel
  ;;  core.async channel to receive `event-msg`s
  ;;  (internal or from clients).
  ;; ch-chsk originally
  (def receive-event-messages-channel ch-recv)

  ;; ChannelSocket's send API fn
  ;; (fn [user-id ev] for server>user push.
  ;; chsk-send! originally
  (def send-user-event! send-fn)

  ;; Watchable, read-only (atom {:ws #{_} :ajax #{_} :any #{_}}).
  (def connected-uids connected-uids))

(add-watch
  connected-uids :connected-uids
  (fn [_ _ old new]
    (when (not= old new)
      (println (str "connected uids change: " new)))))

(defn initial-home-page [http-request]
  (html5
    {:lang "en"}
    [:head
     [:meta {:charset "UTF-8"}]
     [:meta {:name    "viewport"
             :content "width=device-width, initial-scale=1"}]
     [:title "this is a title"]]
    [:body
     [:div#sente-csrf-token
      {:data-csrf-token (:anti-forgery-token http-request)}]
     [:div
      [:h1 "original request"]
      (for [[key value] http-request]
        (let [key   (-> key str h)
              value (-> value pprint with-out-str h)]
          ^{:key (str key value)}
          [:div
           [:h1 key]
           [:pre value]]))]
     [:div#app [:h1 "loading..."]]
     (include-js (if dev?
                   "/cljs-out/dev-main.js"
                   "main.js"))]))

(defn login-handler [http-request]
  (println "(login-handler ")
  (pprint http-request)
  (println ")")
  
  (let [{:keys [session parameters]} http-request
        {:keys [user-id]}            parameters]
    {:status 200 :session (assoc session :uid user-id)}))

(defroutes routes
  (GET "/" http-request (initial-home-page http-request))
  (GET  "/chsk"  http-request
    (ring-ajax-get-or-ws-handshake http-request))
  (POST "/chsk"  http-request
    (ring-ajax-post                http-request))
  (POST "/login" http-request
    (login-handler                 http-request))
  (POST "/haste" http-request
    (haste-handler                 http-request))
  (not-found "<p>page not found</p>"))

(def handler
  (-> routes
    (wrap-defaults site-defaults)
    (wrap-resource "public/")
    wrap-keyword-params
    wrap-params
    wrap-content-type
    wrap-anti-forgery
    wrap-session))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sente channel socket section

(defn test-fast-server>user-push
  "push 1 event to all connected users.  Note that this'll be
  fast+reliable even over ajax!"
  []
  (doseq [uid (:any @connected-uids)]
    (send-user-event! uid [:fast-push/is-fast (str "hello " uid)]))
  (send-user-event!
    :sente/all-users-without-uid
    [:fast-push/is-fast (str "hello anon")]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sente event handlers

(defmulti -event-message-handler
  "handle sente `event-message`s"
  :id)

(defn event-message-handler
  "wrapper to potentially add logging, and error-handling"
  [{:as event-message :keys [id ?data event]}]
  (println "(event-message-handler")
  (pprint event-message)
  (println ")")
  (-event-message-handler event-message))

(defmethod -event-message-handler :default
  [{:as event-message
    :keys [event id ?data ring-req ?reply-fn send-fn]}]
  (let [session {:session ring-req}
        uid {:uid session}]
    (println (str "unhandled event: " event))
    (when ?reply-fn
      (?reply-fn {:unmatched-event-as-echoed-from-server event}))))

(defmethod -event-message-handler :example/test-rapid-push
  [event-message]
  (println "about to do rapid push")
  (test-fast-server>user-push))

(defmethod -event-message-handler :hi/there
  [event-message]
  (pprint event-message)
  (println))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sente event router (our event-message-handler loop)

(defonce ?channel-socket-shutdown-fn! (atom nil))

(defn stop-channel-socket-router! []
  (when-let [stop-fn @?channel-socket-shutdown-fn!]
    (println "shutting down currently running channel socket router")
    (stop-fn)
    (reset! ?channel-socket-shutdown-fn! nil)))

(defn start-channel-socket-router! []
  (stop-channel-socket-router!)
  (reset!
    ?channel-socket-shutdown-fn!
    (start-server-chsk-router!
      receive-event-messages-channel
      event-message-handler)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; app state
(defonce app-state (atom []))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; site http server

(defonce ?http-server-shutdown-fn! (atom nil))

(defn stop-http-server! []
  (when (function? @?http-server-shutdown-fn!)
    (println "shutting down currently running server")
    (@?http-server-shutdown-fn! :timeout 100)
    (reset! ?http-server-shutdown-fn! nil)))

(defn start-http-server! [& [port]]
  (stop-http-server!)
  (let [port        (or port 0)
        top-handler (if dev? (wrap-reload (var handler)) handler)

        [port http-server-shutdown-fn!]
        (let [stop-fn (run-server top-handler {:port port})]
          [(:local-port (meta stop-fn)) stop-fn])

        uri (format "http://localhost:%s/" port)]
    (try
      (.browse (java.awt.Desktop/getDesktop) (java.net.URI. uri))
      (catch java.awt.HeadlessException _))
    (println (format "http server running at %s" uri))
    
    (reset! ?http-server-shutdown-fn!
      http-server-shutdown-fn!)))

(defn stop! []
  (stop-channel-socket-router!)
  (stop-http-server!))

(defn start! [& [port]]
  #_(start-channel-socket-router!)
  (start-http-server! port))

(defn -main [& [port :as argv]]
  (println "starting server")
  (start!)
  (println "server started"))
