(ns ^:figwheel-hooks fyi.tuwo.compotest.client
  (:refer-clojure :exclude [atom])
  (:require-macros
   [cljs.core.async.macros :refer [go go-loop]])
  (:require
   [reagent.dom :refer [render]]
   [reagent.core :refer [atom]]
   [cljs.core.async :refer [<! >! put! chan]]
   [taoensso.encore
    :refer-macros [have have?]
    :refer [format]]
   [taoensso.sente :refer
    [cb-success?
     make-channel-socket-client!
     start-client-chsk-router!
     chsk-disconnect!
     chsk-reconnect!
     ajax-lite]
    :rename
    {start-client-chsk-router! start-client-channel-socket-router!
     chsk-disconnect!          channel-socket-disconnect!
     chsk-reconnect!           channel-socket-reconnect!}]
   [clojure.pprint :refer [pprint]]))

(enable-console-print!)

(defonce web-ui-state
  (atom {:player-id (str (random-uuid))
         :?csrf-token
         (when-let [el (.getElementById js/document "sente-csrf-token")]
           (.getAttribute el "data-csrf-token"))}))

#_(let [{:keys [;; IChSocket implementer. You can usu. ignore this.
                chsk

                ;; core.async channel to receive `event-msg`s (internal
                ;;  or from clients). May `put!` (inject) arbitrary
                ;;  `event`s to this channel.
                ch-recv

                ;;(fn [event & [?timeout-ms ?cb-fn]]) for
                ;;  client>server send.
                send-fn

                ;; Watchable, read-only
                ;; (atom {:type _ :open? _ :uid _ :csrf-token _}).
                state]}
        (make-channel-socket-client!
          "/chsk"
          (:?csrf-token @web-ui-state)
          {:type :auto})]

    (def channel-socket
      (do
        (swap! web-ui-state assoc :channel-socket-state chsk)
        chsk))

    ;; ChannelSocket's receive channel
    ;; core.async channel to receive `event-msg`s (internal
    ;;  or from clients). May `put!` (inject) arbitrary
    ;;  `event`s to this channel.
    (def receive-event-messages-channel ch-recv)

    ;; ChannelSocket's send API fn
    ;;(fn [event & [?timeout-ms ?cb-fn]]) for
    ;;  client>server send.
    (def socket-send! send-fn)

    ;; Watchable, read-only atom
    (def channel-socket-state state))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; received data handlers

(defn handle-socket-data [socket-data]
  (println "handling socket data by printing it:")
  (pprint socket-data))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sente event handlers
(defmulti -event-message-handler
  "multimethod to handle sente `event-msg`s"
  :id)

(defn event-message-handler
  [{:as event-message :keys [id ?data event]}]
  (println "(event-message-handler")
  (pprint event-message)
  (println ")")
  (-event-message-handler event-message))

(defmethod -event-message-handler :default
  [{:as event-message :keys [event]}]
  (println (format "unhandled event: %s" event)))

(defmethod -event-message-handler :chsk/state
  [{:as event-message :keys [?data]}]
  (let [[old-state-map new-state-map] (have vector? ?data)]
    (if (:first-open? new-state-map)
      (println (format "channel socket successfully established!: %s"
                 new-state-map))
      (println (format "channel socket state change: %s"
                 new-state-map)))))

(defmethod -event-message-handler :chsk/recv
  [{:as event-message :keys [?data]}]
  (println (format "push from server: %s" ?data))
  (when ?data
    (handle-socket-data ?data)))

(defmethod -event-message-handler :chsk/handshake
  [{:as event-message :keys [?data]}]
  (let [[?uid ?csrf-token ?handshake-data] ?data]
    (println (format "handshake: %s" ?data))
    (println "entire event:")
    (pprint event-message)))

(defmethod -event-message-handler :chsk/ping
  [event-message]
  (println "received ping!")
  (pprint event-message)
  (println))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sente channel socket router
(defonce ?channel-socket-shutdown-fn! (atom nil))

(defn stop-channel-socket-router! []
  (when (fn? ?channel-socket-shutdown-fn!)
    (println "shutting down currently running channel socket router")
    (?channel-socket-shutdown-fn!)
    (reset! ?channel-socket-shutdown-fn! nil)))

#_(defn start-channel-socket-router! []
    (stop-channel-socket-router!)
    (reset!
      ?channel-socket-shutdown-fn!
      (start-client-channel-socket-router!
        receive-event-messages-channel
        event-message-handler)))

(defn login-request [web-ui-state]
  (let [{:keys [?csrf-token player-id]} @web-ui-state]
    {:method  :post
     :headers {:X-CSRF-Token ?csrf-token}
     :params  {:user-id player-id}}))

#_(defn login-player! [web-ui-state]
    (println "(login-player!")
    (pprint @web-ui-state)
    (println ")")
    (let [socket-request (login-request web-ui-state)]
      (println "socket-request =")
      (pprint socket-request)
      (ajax-lite "/login"
        socket-request
        (fn [ajax-response]
          (println "ajax login response:")
          (pprint ajax-response)
          (println "assuming it was successful!")
          (channel-socket-reconnect! channel-socket)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UI

(defn player-id-section [web-ui-state]
  [:div#player-id-section
   [:h1 "player id"]
   [:div (:player-id @web-ui-state)]
   [:button
    #_{:on-click #(login-player! web-ui-state)}
    "login (disabled)"]])

(defn called-numbers-section [web-ui-state]
  [:div#called-numbers-section
   [:h1 "called numbers"]
   [:button
    "call a number"]
   (if-let [called-numbers (:called-numbers @web-ui-state)]
     (for [called-number called-numbers]
       ^{:key called-number}
       [:span.called-number called-number])
     [:span "no numbers called yet"])])

(defn home [web-ui-state]
  [:div
   [:h1 "hello from reagent"]
   #_[player-id-section web-ui-state]
   [called-numbers-section web-ui-state]])

(defn mount [web-ui-state]
  (render [home web-ui-state] (. js/document (getElementById "app"))))

(defn ^:after-load re-render []
  (mount web-ui-state))

(defn start! []
  #_(start-channel-socket-router!))

(defonce start-up
  (do
    #_(start!)
    (mount web-ui-state)
    true))
